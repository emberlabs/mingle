(function () {
  'use strict';

  const gulp = require( 'gulp' );

  // https://www.npmjs.com/package/gulp-load-plugins
  const plugins = require('gulp-load-plugins')(
    {
      overridePattern: true,
      replaceString: /^gulp(-|\.)/,
      // the glob(s) to search for
      pattern: [ 'autoprefixer', 'cssnano', 'browser-sync', 'gulp-*'],
    }
  );
  // console.log( plugins );

  // Config
  const config = {
    devMode : 'html', // wp | html
    paths : {
      project: 'localhost/projects/UpWork/Jack-K/20200229_mingle/',

      css: {
        input: {
          custom: '../src/sass/style.sass',
          vendors: '../src/vendors/**/*.css',
        },
        output: '../dist/css/',
        watch: {
          custom: [
            '../src/sass/**/*.sass',
            '../components/**/*.sass',

            // '../src/sass/utils/**/*.sass',
            // '../src/sass/front/**/*.sass',
            // '../src/sass/style.sass',
            // '../src/sass/sections/**/*.sass',
            // '../template-pages/**/*.sass',
          ],
          vendors: [
            '../src/vendors/**/*.css',
          ]
        },
      },

      js: {
        input: {
          custom: [
            '../src/js/_global_objects.js',
            '../src/js/helpers/**/*.js',
            '../src/js/components/**/*.js',
            '../src/components/**/*.js',
            '../components/**/*.js',
            '../src/js/app.js',
          ],
          vendors: '../src/vendors/**/*.js'
        },
        output: '../dist/js/',
        watch: {
          custom: [
            '../src/js/**/*.js',
            '../src/components/**/*.js',
            '../components/**/*.js',
          ],
          vendors: [
            '../src/vendors/**/*.js',
          ]
        }
      },

      includes: {
        input: [
          // '../components/pages/**/*.html',
          '../components/pages/page5.html',
          '../components/pages/page6.html',
          '../components/pages/page7.html',
          '../components/pages/page8.html',
        ],
        output: '../dist/',
        watch: {
          php: [
            '../components/**/*.php',
            '../template-pages/**/*.php',
            '../templates/**/*.php',
          ],
          html: [
            '../components/pages/*.html',
            // '../components/pages/*.html',
            // '../components/**/*.component',
            // '../template-pages/**/*.html',
          ]
        }
      }

    }
  };

  // Tasks
  const bsReload = require ( './tasks/task.bs-reload.js' )( plugins );
  const bs = require ( './tasks/task.bs.js' )( plugins, config );
  const css = require ( './tasks/task.css.js' )( gulp, plugins, config );
  const cssVendors = require ( './tasks/task.css-vendors.js' )( gulp, plugins, config );
  const cssPurge = require ( './tasks/task.css-purge.js' )( gulp, plugins, config );
  const includes = require ( './tasks/task.includes.js' )( gulp, plugins, config );
  const jsConcat = require ( './tasks/task.js-concat.js' )( gulp, plugins, config );
  const jsVendors = require ( './tasks/task.js-vendors.js' )( gulp, plugins, config );
  const js = require ( './tasks/task.js.js' )( gulp, plugins, config );
  const watch = require ( './tasks/task.watch.js' )( gulp, config, bsReload, css, includes );

  let build;
  if ( config.devMode === 'wp' ) {
    build = gulp.series ( gulp.parallel(
      css,
      cssVendors,
      js,
      jsVendors,
    ) );
  }
  if ( config.devMode === 'html' ) {
    build = gulp.series ( gulp.parallel(
      includes,

      css,
      // cssVendors,
      // js,
      // jsVendors,
    ) );
  }

  const dev = gulp.series ( build, gulp.parallel( watch, bs ) );

  exports.jsVendors = jsVendors;
  exports.cssVendors = cssVendors;
  exports.build = build;
  exports.default = dev;
  exports.jsConcat = jsConcat;
  exports.cssPurge = cssPurge;

}());