module.exports = function ( gulp, plugins, config) {
  return function jsConcat( done ){
    gulp
      .src([
        // '../src/colorlib_theme/js/vendor/jquery-2.2.4.min.js',
        // '../src/colorlib_theme/js/vendor/bootstrap.min.js',
        // '../src/colorlib_theme/js/plugins/**/*.js',
        // '../src/colorlib_theme/js/main.js',
      ])
        .pipe( plugins.concat( 'script.min.js' ) )
        // .pipe( plugins.uglify()) // production mode
        .pipe (plugins.size(
          {
            showFiles: true
          }
        ))
        .pipe( gulp.dest( '../dist/theme/js/' ) );
    done();
  };
}