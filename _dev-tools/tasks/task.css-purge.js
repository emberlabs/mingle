module.exports = function ( gulp, plugins, config ) {

  class TailwindExtractor {
    static extract(content) {
      return content.match(/[A-z0-9-:\/]+/g);
    }
  }

  return function cssPurge ( done ) {
    gulp.src( config.paths.css.output + 'style.min.css' )
      .pipe( plugins.purgecss(
        {
          content: [
            '../template-pages/**/*.php',
            '../components/**/*.php',
            // './components/**/*.component'
          ],
          // defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || [],
          extractors: [
            {
              extractor: TailwindExtractor,
              extensions: ['php']
            }
          ],
          whitelist: [ 'is-active', 'hidden' ]
        }
      ) )
      .pipe( plugins.rename(
        'style.purged.min.css'
        // {
        //   suffix: '.purged'
        // }
      ) )
      .pipe( plugins.size(
        {
          showFiles: true
        }
      ))
      .pipe(gulp.dest( config.paths.css.output ))
      .pipe( plugins.browserSync.stream() );

    done();
  };
};