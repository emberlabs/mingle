module.exports = function ( gulp, plugins, config ) {
  return function cssVendors ( done ) {
    gulp.src( config.paths.css.input.vendors )
      .pipe( plugins.concat( 'vendors.min.css' ))
      .pipe( plugins.postcss(
        [
          plugins.cssnano()
        ]
      ) )
      .pipe( plugins.size(
        {
          showFiles: true
        }
      ))
      .pipe(gulp.dest( config.paths.css.output ));

    done();
  };
};