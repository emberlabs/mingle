module.exports = function ( gulp, config, bsReload, css, includes ) {
  return function watch ( done ) {

    // CSS FRONT CUSTOM
    gulp.watch (
      config.paths.css.watch.custom,
      // css
      gulp.series(
        css,
        // cssPurge
      )
    );

    // CSS PURGE
    // gulp.watch(
    //   config.paths.css.output + 'style.min.css',
    //   cssPurge
    // );

		// CSS FRONT VENDORS
    // gulp.watch(
    //   config.paths.css.watch.vendors,
    //   cssVendors
    // );

    // JS
    // gulp.watch (
    //   config.paths.js.watch.custom,
    //   gulp.series( js )
    // );

    // JS VENDORS FRONT
    // gulp.watch(
    //   config.paths.js.watch.vendors,
    //   jsVendors
    // );

    // INCLUDES
    gulp.watch(
      config.devMode === 'html' ? config.paths.includes.watch.html : config.paths.includes.watch.php,
      config.devMode === 'html' ? gulp.series( includes, bsReload ) : gulp.series( bsReload )
    );

		// gulp.watch( [ config.paths.scripts.src, '../src/js/**/*.js','./gulpfile.js'], gulp.series(scriptsLint, scripts) );
		// gulp.watch('./assets/img/**/*', images);

  };
}