module.exports = function ( plugins, config ) {
  return function bs( done ){

    if( config.devMode ==='html' ){
      plugins.browserSync.init({
        server: {
          baseDir: '../dist/'
        },
        port: 3000
      });
      done();
    }

    if( config.devMode==='wp'){
      plugins.browserSync.init({
        proxy: config.paths.project,
        files: ['./*.php', './*.css', './inc/**', './js/*.js', './css/*.css'],
        server: false,
        timestamps: true,
        injectChanges: true, //Inject CSS changes
        notify: false,
        // tunnel: true,
        // tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
      });
      done();
    }

  };
}