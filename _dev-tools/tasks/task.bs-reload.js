module.exports = function ( plugins ) {
  return function bsReload( done ){
    plugins.browserSync.reload();
    done();
  };
}