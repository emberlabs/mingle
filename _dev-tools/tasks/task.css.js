module.exports = function ( gulp, plugins, config ) {
  return function css ( done ) {
    gulp
      .src( config.paths.css.input.custom )
      .pipe( plugins.plumber() )
      // .pipe( plugins.sourcemaps.init() )
      .pipe( plugins.sassGlob() )
      .pipe( plugins.sass( { outputStyle: 'expanded' } ) )
      // .pipe( plugins.purgecss(
      //   {
      //     content: [
      //       '../template-pages/**/*.php',
      //       '../components/**/*.php',
      //       // './components/**/*.component'
      //     ],
      //     whitelist: [ 'is-active', 'hidden' ],
      //   }
      // ) )
      .pipe( plugins.postcss(
        [
          // require( 'tailwindcss' ),
          plugins.autoprefixer(
            {
              // browsers: ['last 2 version']
            }
          ),
          plugins.cssnano()
        ]
      ) )
      // .pipe( plugins.sourcemaps.write('.') )
      .pipe( plugins.rename( { suffix: '.min' } ) )
      .pipe( plugins.size(
        {
          showFiles: true
        }
      ))
      .pipe( gulp.dest( config.paths.css.output ) )
      .pipe( plugins.browserSync.stream() );
      done();
  };
}