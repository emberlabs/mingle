module.exports = function ( gulp, plugins, config) {
  return function js( done ){
    gulp
      .src( config.paths.js.input.custom )
        .pipe( plugins.concat( 'app.min.js' ) )
        // .pipe( plugins.uglify()) // production mode
        .pipe (plugins.size(
          {
            showFiles: true
          }
        ))
        .pipe( gulp.dest( config.paths.js.output ))
        .pipe( plugins.browserSync.reload({ stream: true }) );
    done();
  };
}