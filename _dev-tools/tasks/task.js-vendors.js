module.exports = function ( gulp, plugins, config ) {
  return function jsVendors ( done ) {

    gulp.src( config.paths.js.input.vendors )
      .pipe( plugins.concat( 'vendors.min.js' ) )
      .pipe( plugins.uglify() )
      .pipe (plugins.size(
        {
          showFiles: true
        }
      ) )
      .pipe( gulp.dest( config.paths.js.output ) )
      .pipe( plugins.browserSync.reload( { stream: true } ) );
    done();
  };
};