// https://www.npmjs.com/package/gulp-file-include
module.exports = function ( gulp, plugins, config) {
  return function includes ( done ) {
    gulp
      .src( config.paths.includes.input )
      .pipe( plugins.plumber() )
      .pipe( plugins.fileInclude({
        prefix: '@@',
        basepath: '../src/',
      }))

      //BUILD MODE
      // .pipe( plugins.htmlmin(
      //   {
      //     collapseWhitespace: true,
      //     removeComments: true
      //   }
      // ))
      .pipe( plugins.size(
        {
          showFiles: true
        }
      ))
      .pipe( gulp.dest( config.paths.includes.output ) );
    done();
  };
}